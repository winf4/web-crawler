import argparse
import asyncio
import aiohttp
from urllib.parse import urljoin, urlparse

from bs4 import BeautifulSoup


async def crawler(queue, session):
    """
    Main logic on the page crawler.

    :param queue:
    :param session:
    :return:
    """
    while not queue.empty():
        url, level = await queue.get()

        # Previously visited urls are skipped to save on time
        if url in visited:
            continue

        visited.add(url)

        # if no keywords are added we may skip parsing the last depth pages
        if not keyword and level == max_level:
            continue

        try:
            async with session.get(url) as response:
                # Get page content
                content = await response.read()
                encoding = response.get_encoding()
                html = content.decode(encoding or 'utf-8')

                soup = BeautifulSoup(html, "html.parser")

                # Look for the keyword on the page
                if keyword and keyword in soup.get_text():
                    index = html.find(keyword)
                    start = index - 10 if index - 10 >= 0 else 0
                    end = index + len(keyword) + 10
                    surroundings = html[start:end]
                    pages_with_keyword.add(f"{url} => '{surroundings}'")

                # Loop over found urls
                for link in soup.find_all("a"):
                    href = link.get("href")
                    if href:
                        next_url = urljoin(url, href)
                        parsed_url = urlparse(next_url)

                        # Check if the found url is on the same domain as the main url
                        if parsed_url.netloc == main_domain:
                            # If current level is max level then no need to go deeper
                            if level == max_level:
                                continue

                            await queue.put((next_url, level + 1))

        except Exception as e:
            print(f"Failed with {url}. Error: {e}")


async def start():
    """
    Function to start the web crawling

    :return:
    """

    # Add the main page to queue
    queue = asyncio.Queue()
    await queue.put((main_url, 0))

    # Start consuming the queue and set up a client session to speed up page fetching
    async with aiohttp.ClientSession(connector=aiohttp.TCPConnector(ssl=False)) as session:
        await crawler(queue, session)

    print(f"Crawled {len(visited)} pages.")
    if keyword:
        print(f"Found {len(pages_with_keyword)} pages with the term {keyword}:")
        for page in pages_with_keyword:
            print(page)


if __name__ == '__main__':
    """
    To use:
    python crawler.py --url=https://www.delfi.ee/ --keyword=uudised --max_level=1

    """
    # Add console arguments
    parser = argparse.ArgumentParser(description='Crawl the web')
    parser.add_argument('--url', metavar='path', required=False,
                        help='Url of the page to crawl')
    parser.add_argument('--keyword', metavar='path', required=False,
                        help='Keyword to search for')
    parser.add_argument('--max_level', metavar='path', required=False,
                        help='Dept of the crawler. Value 0 means only urls on the main site counted')
    args = parser.parse_args()

    main_url = args.url or 'https://www.delfi.ee/'  # main URL to crawl
    keyword = args.keyword or ''  # keyword to look for in the url
    max_level = int(args.max_level) or 0  # Max level to crawl to
    visited = set()  # Keep track of visited URLs
    pages_with_keyword = set()  # Keep track of pages with the keyword in it
    main_domain = urlparse(main_url).netloc

    asyncio.run(start())
