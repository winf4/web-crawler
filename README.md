**Readme for "Web Crawler" code repository**

This code repository contains a Python program for a web crawler. 

The program is designed to crawl the pages of a given website, and retrieve URLs of all pages on that website up to a specified depth. 

Optionally, the program can search for a specified keyword in the content of each page that it visits and keep track of pages that contain the keyword.


The program uses asyncio and aiohttp to make asynchronous HTTP requests to fetch pages and parse them. 

It also uses html5lib to parse the HTML of each page and extract URLs from the page content.

To use the program, you can run it from the command line and pass in the following optional arguments:

```
--url: The URL of the website to crawl. If not provided, it defaults to 'https://www.delfi.ee/'.

--keyword: The keyword to search for in the content of each page. If not provided, the program will not search for any keyword.

--max_level: The maximum depth to crawl. A value of 0 means only the pages on the main site are counted. If not provided, it defaults to 0.
```


Requirements for the "Web Crawler" program in this repository are:

```
Python 3.9 or newer
aiohttp
html5lib
```

To install Python go to https://www.python.org/downloads/

Download the Python vesion you like and install it

Open the terminal and navigate to the project to run the command (If you use virtual environments then create and activate it first. More info in https://docs.python.org/3/library/venv.html)

pip install -r requirements.txt


If done correctly you can now use the program

Example usage:

`python crawler.py --url=https://www.example.com/ --keyword=searchterm --max_level=1
`




